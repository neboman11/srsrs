use std::sync::Arc;

use futures::channel::mpsc::UnboundedReceiver;
use futures::StreamExt;
use tokio::sync::Mutex;

use srs::message::{GameMessage, Radio};

#[derive(Clone)]
pub struct State {
    pub radios: Vec<Radio>,
    pub ptt_active: bool,
    pub radio_index: usize,
    pub unit_id: u32,
}

impl State {
    pub fn new() -> Self {
        Self {
            radios: vec![],
            ptt_active: false,
            radio_index: 1,
            unit_id: 0,
        }
    }

    /// Returns the frequency which is selected for transmitting
    pub fn active_frequency(&self) -> Option<srs::Frequency> {
        self.selected_radio().map(|(_, radio)| srs::Frequency {
            freq: radio.freq,
            modulation: srs::Modulation::AM,
            encryption: srs::Encryption::None,
        })
    }

    pub fn selected_radio(&self) -> Option<(usize, &Radio)> {
        self.radios
            .iter()
            .skip(self.radio_index)
            .next()
            .map(|r| (self.radio_index, r))
    }

    /// Returns the frequency to transmit on if push to talk is held
    pub fn should_transmit_where(&self) -> Option<srs::Frequency> {
        if self.ptt_active {
            self.active_frequency()
        } else {
            None
        }
    }
}

pub async fn state_updater(
    state: Arc<Mutex<State>>,
    mut message_rx: UnboundedReceiver<GameMessage>,
) {
    while let Some(msg) = message_rx.next().await {
        let mut s = state.lock().await;

        s.radios = msg.radios.clone();
        s.unit_id = msg.unit_id;
    }
}
