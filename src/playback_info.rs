use srs::message::Radio;
use srs::VoicePacket;

pub struct PlaybackInfo {
    pub volume: f32,
    pub channel: Vec<f32>,
}

impl PlaybackInfo {
    pub fn get(packet: &VoicePacket, radios: &[Radio]) -> Option<Self> {
        packet_radio(packet, radios).map(|(id, radio)| Self {
            volume: radio.volume,
            channel: if id % 2 == 1 {
                vec![1., 0.]
            } else {
                vec![0., 1.]
            },
        })
    }
}

/// Returns the index of the radio, along with the radio info for the specified voice packet
fn packet_radio(packet: &VoicePacket, radios: &[Radio]) -> Option<(usize, Radio)> {
    packet
        .frequencies
        .iter()
        .map(|freq| {
            radios
                .iter()
                .cloned()
                .enumerate()
                .filter(|(_, radio)| radio.freq == freq.freq as f64)
                .next()
        })
        .next()
        .unwrap_or(None)
}
