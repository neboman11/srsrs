// use std::io;
use std::sync::Arc;

use tokio::sync::Mutex;

use srs::message::{Modulation};

use crate::channel_logger::LogMsg;
use crate::state::State;

pub struct Model {
    pub log_msgs: Vec<LogMsg>,
    pub srs_state: Arc<Mutex<State>>,
}

impl Model {
    pub fn new(srs_state: Arc<Mutex<State>>) -> Self {
        Self {
            log_msgs: vec![],
            srs_state,
        }
    }

    pub async fn set_ptt(self, to: bool) -> Self {
        self.srs_state.lock().await.ptt_active = to;
        self
    }

    pub async fn prev_radio(self) -> Self {
        {
            let mut s = self.srs_state.lock().await;
            s.radio_index = s.radio_index.saturating_sub(1);
        }
        self
    }
    pub async fn next_radio(self) -> Self {
        self.srs_state.lock().await.radio_index += 1 as usize;
        self
    }

    pub async fn radio_add_mhz(self, amount: f64) -> Self {
        {
            let mut s = self.srs_state.lock().await;
            let radio_index = s.radio_index;

            match s.radios[radio_index].modulation {
                Modulation::AM => {
                    if s.radios[radio_index].freq + (amount * 1000000.0) <= 400000000.0 && s.radios[radio_index].freq + (amount * 1000000.0) >= 1000000.0 {
                        s.radios[radio_index].freq += amount * 1000000.0;
                    }
                },
                Modulation::FM => {
                    if s.radios[radio_index].freq + (amount * 1000000.0) <= 76000000.0 && s.radios[radio_index].freq + (amount * 1000000.0) >= 1000000.0 {
                        s.radios[radio_index].freq += amount * 1000000.0;
                    }
                },
                _ => {},
            }
        }
        self
    }
}
