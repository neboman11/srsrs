use crate::channel_logger::LogMsg;

#[derive(Clone, Debug)]
pub enum Msg {
    KeyInput(termion::event::Key),
    NewLogMsg(LogMsg),
    PttOn,
    PttOff,
    NextRadio,
    PrevRadio,
    RadioAdd01MHz,
    RadioAdd1MHz,
    RadioAdd10MHz,
    RadioSub01MHz,
    RadioSub1MHz,
    RadioSub10MHz,
    Exit,
}

pub enum Cmd {
    Loopback(Msg),
}
