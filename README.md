# SRSRS

An alternative client for DCS Simple Radio Standalone. The primary goal is to make
a client that runs on linux to make SRS usable when playing the game in proton.

Based on the `srs` crate from the excellent https://github.com/rkusa/DATIS project


# Usage

Manually install the DCS-SRS scripts into the game by following the instructions here https://github.com/ciribob/DCS-SimpleRadioStandalone/tree/1.9.0.2/install-build

## Initialise and update the submodules

```
git submodule init && git submodule update
```

## Configure key bindings

To set your keybindings, execute `cargo run -- configure` and then use the
numbers to select which controls to bind. For keyboard keys, the white window
that pops up must be focused for keys to register, for hotas or gamepad
controls, you can just press the key right away.

## Run srsrs

Run `srsrs` using `cargo run -- run <url> <coalition>`

The coalition parameter is `red` or `blue` depending on which side you are on in game.

If you want to specify a port that is not the default 5002, add the `-p <port>` flag

Once the program is running and you are in a plane, you should see a list of
radios. However, the in-game srs overlay will still show not connected, this is
normal as srsrs currently does not reply to the game.


# Features and limitations

- [x] Receiving and playing radio messages
- [x] Receiving radio configuration from the game
- [x] Voice transmission
- [ ] Line of sight simulation
- [ ] Replies to the in-game portion (It will always show "no client connected")
